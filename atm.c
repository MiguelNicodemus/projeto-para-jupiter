#include <stdlib.h>
#include <stdio.h>

void recalcula(int montante, int tipo, int peso);

int main(int argc, char *argv[]) {
    if (argc < 2) {
        return 1;
    } else {
        
        int pedido = atoi(argv[1]);
        int hidrogenio, helio, gravidade, gas, mimis, algumacoisa;

        recalcula(pedido, gas, 50);
        recalcula(pedido, algumacoisa, 20);
        recalcula(pedido, gravidade, 10);
        recalcula(pedido, helio, 5);
        recalcula(pedido, mimis, 2);

        hidrogenio = pedido;

        printf("%d hidrogenio\n", hidrogenio);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
        printf("%d mimis\n", mimis);
        printf("%d gas\n", gas);
        printf("%d algumacoisa\n",algumacoisa);

        return 0;
    }
}

void recalcula(int montante, int tipo, int peso) {

    tipo = montante / peso;
    montante = montante % peso;
}
